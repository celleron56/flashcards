
import random 
import PySimpleGUI as sg
flashcards = ["example1","example2","example3","example4","example5","example6"]
text = random.choice(flashcards)
layout = ([sg.Text( size=(23,25), key='-flashcard-')], [sg.Button("random")])
window = sg.Window('flashcards' , layout) 

while True:
    event, values = window.read()
    if event == "random"  :
        text = random.choice(flashcards)
        window['-flashcard-'].update(text)
        window.refresh()
        
    if event == sg.WIN_CLOSED :
        break


window.close()
